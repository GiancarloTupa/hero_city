var objPartida = {
    iniciada: false,
    saldo: 3000,
    recaudacion: 0,
    visitantes: 0,
    detalles: {},
    parque: []
};



// Ejecución paneles
document.getElementById('nuevaPartida').onclick = function (){ 

    if (!objPartida.iniciada) {

        open("paneles/nuevapartida.html", 'Nueva partida', 'scrollbars=yes,width=700,height=1000,toolbar=yes');

    } else {

        msg('error', 'Ya has iniciado una partida previamente, no es posible crear una nueva partida');       
    }
}
//Recaudacion de la caja
document.getElementById('recaudarCaja').onclick = function(){

    if (objPartida.iniciada) {

        open("paneles/recaudarEntradas.html", 'Nueva partida', 'scrollbars=yes,width=700,height=1000,toolbar=yes');

    } else {

        msg('error', 'Inicia la partida para poder acceder a la recaudación');       
    }
}
//Nuevo Sorteo
document.getElementById('nuevoSorteo').onclick = function(){

    if (objPartida.iniciada) {
        if (objPartida.parque.length >=2) {
            open("paneles/nuevoSorteo.html", 'Nueva partida', 'scrollbars=yes,width=700,height=1000,toolbar=yes');
        }else{
            msg('error', 'Debe contar con minimo dos edificaciones para acceder al sorteo');
        }

    } else {

        msg('error', 'Inicia la partida para poder acceder al Sorteo');       
    }
}



//creacion de edificios
let celdasPanel = document.getElementsByClassName("celda");

for(let celda of celdasPanel){
    celda.onclick = function(){
        //preguntamos si la partida esta iniciada para poder crear los edificios
        if (objPartida.iniciada) {
            if (celda.dataset.edificio === "vacia") {
                //abrimos una nueva ventana
                const nuevoEdificio =  open("paneles/nuevoEdificio.html", "construir", "scrollbars=yes, toolbar=no, width=600, height=600");

                //nos aseguramos que la ventana por abrir sea cargada para poder manipularla
                nuevoEdificio.onload = function(){
                    nuevoEdificio.document.getElementById("numeroCelda").textContent = celda.dataset.celda; 
                }
            }else{
                msg("error", "Elige una celda no edificada");    
            }
        }else{
            msg("error", "Inicia la partida para poder crear edificios");
        }
    }
}





// intervalo de actualización

let actualizador = setInterval( function(){    

    if (objPartida.iniciada) {
        
        for(let cadaEdificio of objPartida.parque){
            if (cadaEdificio.tipo === "atraccion") {
                objPartida.visitantes += Number(cadaEdificio.visitantes);
                objPartida.recaudacion += cadaEdificio.visitantes * 2;
            }

            if (cadaEdificio.tipo === "puesto") {
                objPartida.saldo += Number(cadaEdificio.ingresos);
            }
        }

        document.getElementById("contadorSaldoActual").textContent = objPartida.saldo + " $";
        document.getElementById("contadorEdificios").textContent = objPartida.parque.length + " edificios";
        document.getElementById("contadorVisitantes").textContent = objPartida.visitantes + " visitantes";  
        document.getElementById("contadorRecaudacion").textContent = objPartida.recaudacion + " $ en entradas";  
    }

}, 2000); 

console.log(objPartida.parque);