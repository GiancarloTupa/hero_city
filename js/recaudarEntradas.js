if (!opener) {
    alert("Debe ingresar desde el panel central");
    location.assign("../index.html");//redirigir al panel deseado
}

// gestión de venta de unidades
document.getElementById('recaudar').onclick = function() {

    let beneficioTotal = opener.objPartida.recaudacion - 200;

    opener.objPartida.saldo += beneficioTotal;    

    opener.objPartida.recaudacion = 0;

    opener.msg('success', 'Has ingresado ' + beneficioTotal + '$');

    window.close();

}

