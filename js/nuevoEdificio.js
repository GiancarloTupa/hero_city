if (!opener) {
    alert("Debe ingresar desde el panel central");
    location.assign("../index.html");//redirigir al panel deseado
}


class Edificio {

	constructor(celda, nombre){
		this._celda = celda;
		this._nombre = nombre;
	}

	inicializar(){
		let celdas = opener.document.getElementsByClassName("celda");

		for (let cadaCelda of celdas) {
			if(cadaCelda.dataset.celda === this._celda){
				cadaCelda.dataset.edificio = this._nombre;
			}
		}
	}

	get tipo(){
		return this._tipo;
	}
}

class Atraccion extends Edificio{
	
	constructor(celda, nombre, visitantes){
		super(celda, nombre);
		this._visitantes = visitantes;
		this._tipo = "atraccion";
	}	

	get visitantes(){
		return this._visitantes;
	}
}

class Puesto extends Edificio{

	constructor(celda, nombre, ingresos){
		super(celda, nombre);
		this._ingresos = ingresos;
		this._tipo = "puesto";
	}

	get ingresos(){
		return this._ingresos
	}
}


let edificios = document.getElementsByClassName("edificio");

for(let cadaEdificio of edificios){
	cadaEdificio.onclick = function(){

		//comprobar si tiene dinero
		if (opener.objPartida.saldo >= cadaEdificio.dataset.coste ) {

			//saber si estamos en una atraccion o un puesto
			if (cadaEdificio.dataset.tipo === "puesto") {


				//obtener los datos necesarios para crear el puesto
				let celda = document.getElementById("numeroCelda").textContent;
				let nombre = cadaEdificio.dataset.nombre;
				let ingresos = cadaEdificio.dataset.ingresos;

				//crear un nuevo edificio e inicializarlo (hacer que se dibuje en el mapa)
				let puesto = new Puesto(celda, nombre, ingresos);
				puesto.inicializar();
				
				//restar dinero por cada edificio
				opener.objPartida.saldo -= cadaEdificio.dataset.coste;

				//insertar en el objPartida.parque
				opener.objPartida.parque.push(puesto);

				opener.msg("success", "Puesto creado");

				window.close();
			}

			if (cadaEdificio.dataset.tipo === "atraccion") {

				//obtener los datos necesarios para crear la atraccion
				let celda = document.getElementById("numeroCelda").textContent;
				let nombre = cadaEdificio.dataset.nombre;
				let visitantes = cadaEdificio.dataset.visitantes;

				//crear un nuevo edificio e inicializarlo (hacer que se dibuje en el mapa)
				let atraccion = new Atraccion(celda, nombre, visitantes);
				atraccion.inicializar();

				//restar dinero por cada edificio
				opener.objPartida.saldo -= cadaEdificio.dataset.coste;
				
				//insertar en el objPartida.parque
				opener.objPartida.parque.push(atraccion);

				opener.msg("success", "Atraccion creada");

				window.close();
			}


		}else{
			msg("error", "Saldo insuficiente (te quedan: "+opener.objPartida.saldo+")");
		}
	}
}