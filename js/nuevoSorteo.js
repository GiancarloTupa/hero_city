if (!opener) {
    alert("Debe ingresar desde el panel central");
    location.assign("../index.html");//redirigir al panel deseado
}


//obteniendo el numero ganador y perdedor para el sorteo
function obtenerNumeroAleatorio(min, max) {
  return Math.floor(Math.random() * (max - min)) + min;
}
let numeroGanador, numeroPerdedor;

numeroGanador = obtenerNumeroAleatorio(1,6);
numeroPerdedor = obtenerNumeroAleatorio(1,6);

//nos aseguramos que ambos numeros no sean iguales

while(numeroPerdedor === numeroGanador){
	numeroPerdedor = obtenerNumeroAleatorio(1,6);
}

// obtenemos los datos del array parque
let datos = opener.objPartida.parque;
//console.log(datos);


//obtenemos el array con los datos de los botones
let botones = document.getElementsByTagName('button');
let numeroElegido;

// variable para recorrer las celdas del paner principal
let celdas;
//recorremos los botones y obtenemos el valor de textcontent
for(let cadaNumero of botones){
	cadaNumero.onclick = function(){
		numeroElegido = cadaNumero.textContent;

		//numero ganador
		if (numeroGanador == numeroElegido) {
			opener.objPartida.saldo += 10000;
			opener.msg('success', 'Ud ha obtenido un premio de 10000€, felicidades!!!');
			window.close();
		//numero perdedor
		}else if (numeroPerdedor == numeroElegido){
			
			//obtenemos el array de las celdas del paner principal
			celdas = opener.document.getElementsByClassName("celda");
			
			//cambiamos dato celda por vacio
			for (let i = 0; i < 2; i++) {
				for (let cadaCelda of celdas) {
					if(cadaCelda.dataset.celda === opener.objPartida.parque[i]._celda){
						cadaCelda.dataset.edificio = "vacia";
					}
				}
			}
			
			//eliminamos los datos del array parque
			opener.objPartida.parque.splice(0, 2); 
			opener.msg('error', 'Ud ha perdido dos edificios, mala suerte!!!');
			window.close();
		//cualquier otro numero
		}else{
			opener.msg('success', 'No ganas, pero tampoco pierdes!!!. Vuelve a intentarlo!!!');
			window.close();
		}
	}
}


console.log("El premio se encuentra tras la casilla: "+ numeroGanador, "el terremoto tras la casilla: " +numeroPerdedor);	


